package com.yomahub.tlog.example.dubbo.service;

public interface DemoServiceC {

	String sayHello(String name);

	String sayMorning(String name);

	String sayEvening(String name);
}
