package com.yomahub.tlog.base.test.domain;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author hutaishi@qq.com
 * @date 2020-11-22
 */
@Accessors(chain = true)
@Data
public class User {
    private Long id;
    private String username;
    private String nickname;
}
