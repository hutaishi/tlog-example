package com.yomahub.tlog.base.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hutaishi@qq.com
 * @date 2020-11-22
 */
@SpringBootApplication
public class TLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(TLogApplication.class, args);
    }
}
