package com.yomahub.tlog.base.test;

import com.yomahub.tlog.base.test.domain.User;
import com.yomahub.tlog.base.test.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author hutaishi@qq.com
 * @date 2020-11-22
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class TLogApplicationTests {

    @Autowired
    private UserService userService;

    /**
     * 测试TLogAspect注解自定义标签支持键值对的形式
     */
    @Test
    public void testAspectLogAop() {
        User user = new User().setId(1L).setUsername("hello").setNickname("world");
        userService.updateUser(user);
    }

}