package com.yomahub.tlog.base.test.service;

import com.yomahub.tlog.base.test.domain.User;
import com.yomahub.tlog.core.annotation.TLogAspect;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author hutaishi@qq.com
 * @date 2020-11-22
 */
@Slf4j
@Service
public class UserService {

    @TLogAspect({"user.id", "user.username", "user.nickname"})
    public void updateUser(User user) {
        log.info("更新用户数据params={}", user);
    }

}
