package com.yomahub.tlog.example.dubbo.service.impl;

import com.yomahub.tlog.example.dubbo.service.DemoService;
import com.yomahub.tlog.example.dubbo.service.DemoServiceC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("demoServiceC")
public class DemoServiceCImpl implements DemoServiceC {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public String sayHello(String name) {
        log.info("node C");
        return "hello," + name;
    }

    @Override
    public String sayMorning(String name) {
        log.info("node C");
        return "morning," + name;
    }

    @Override
    public String sayEvening(String name) {
        log.info("node C");
        return "evening," + name;
    }
}
