package com.yomahub.tlog.example.dubbox;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource(locations = {"classpath:/applicationContext.xml"})
public class Runner {

    static {AspectLogEnhance.enhance();}

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }
}
